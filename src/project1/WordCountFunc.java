/* Anakaren Borjon
 * This is the code for the function in which 
 * the text file will go ahead and be read 
 * and the total number of words, different number of words, and the number of each
 * different word will be printed. 
*/

package project1;

import java.util.*;
import java.io.*;
/*
 * WordCountFunc is the function where the gui will call on in order to
 * go ahead and count the number of words and the different number of words there are.
 */

public class WordCountFunc 
{

 
public  String word(File file) throws FileNotFoundException
{ 
	//Scanner goes ahead and reads the file that was selected 
	 //Hashmap is used to count the different number of words
	 //
	HashMap<String, Integer> map = new HashMap<String, Integer>();	
	Scanner File = new Scanner(file); 
	
   
    // The counter is the set equal to 0 for the total number of words
   
	
	int wordc =0;
	 
	 // While the file has words in it 
	 // they will be counted (This will not count white space/ punctuation)
	 // and then it will return the value to main
	 //
	
   while (File.hasNext()) {
      
       String word = File.next();
      
       
   
    //     This counts the total number of words
  
     
       
      //  Word is being replaced by words so that punctuation
        //and white space wont be counted 
        
       
   	String Words = word.replaceAll("[\\W\\d]", "").toLowerCase();
   	//if statment states that if there are words in the hashmap
   	 //each one of those words get added every time that same word
   	 // appears. 
   	  
   	 
   	
	if(map.containsKey(Words))
	{
		map.put(Words, map.get(Words) + 1);
		
				
	}
	else 
	{
		map.put(Words, 1);
	}
	  
       
   }
   

    //This will go ahead and print out the words and the amount of times
    // that word was found
   
   
   for (Map.Entry<String, Integer> entry : map.entrySet())
   {
       System.out.println(entry.getKey() + ": " + entry.getValue());
       wordc = wordc + entry.getValue();
   }
   
   System.out.println("The number of unique words are " + map.size());
   System.out.println("The number of Total words are " + wordc);

   return null;
  
   
}






}
