package project1;
/* Anakaren Borjon
* This is the function part of the GUI program
*  will be using javax.Swing to generate
*Buttons and file chooser.
*/

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;



public class Gui extends JFrame implements ActionListener {
	// I used JLabel to create the label for the Frame
	// I also used Jbutton to create both the count and load button
	// I used Jfilechooser in order to have a pop up of the files
	// in the computer where the user will choose a file to get the
	// text from

	private JLabel sometext = new JLabel("Word Count");
	private JButton button = new JButton("Load");
	private JFileChooser choose = new JFileChooser(" File");
	private JButton button2 = new JButton("Count");
	public WordCountFunc count = new WordCountFunc();
	public JLabel output = new JLabel("Output:  ");
	

	public Gui() {
		// This is where i set the bounds for the Frame to get the size
		// That i needed it to be. It is also where i set the bounds
		// for the count and load button, and the file chooser.
		// I was also able to lay out the buttons how i wanted to
		// by setting the bounds.

	
       
		setVisible(true);
		this.setBounds(700, 700, 700, 500);
		this.setTitle("Word Count Calculator");
		this.getContentPane().setLayout(null);

		sometext.setBounds(5, 10, 100, 40);
		this.getContentPane().add(sometext);

		button.setBounds(40, 70, 80, 40);
		this.getContentPane().add(button);
		button.setActionCommand("Load");
		button.addActionListener(this);

		button2.setBounds(40, 200, 80, 40);
		button2.setEnabled(false);
		this.getContentPane().add(button2);
		button2.addActionListener(this);
	
		output.setBounds(40, 225, 170, 40);
		this.getContentPane().add(output);
		
		
	      
		
		
		
		

	}

	public void actionPerformed(ActionEvent e) {

		// This if an else statement makes it to that
		// i can enable the count button and file button
		// after the load button has been pressed

	

		this.button.setText("Load");
		
		
		
		if (e.getSource() == button) {

			int r = choose.showOpenDialog(null);

			if (r == JFileChooser.APPROVE_OPTION) {
				
				button2.setEnabled(true);
				button.setEnabled(false);
				

			}
			else {
				button2.setEnabled(true);
				button.setEnabled(false);
			}

		}
		if (e.getSource() == button2) {
			//After the count button is pushed
			//it goes ahead and counts the number of words in the selected file,the unique words and the number
			//of occurrences. 
			//it then displays it in the console and in the text file that i have selected to put the answer in.
			
			
			
			try {
				count.word(choose.getSelectedFile());
				PrintStream myconsole = new PrintStream(new File("C:\\Users\\Ana\\Desktop\\Anakaren\\Test\\Practice.txt "));
				System.setOut(myconsole);
				myconsole.println(count.word(choose.getSelectedFile()));
				
			} catch (FileNotFoundException e1) {

				e1.printStackTrace();
			}
			
			
			
		}
		
		
	}
	
	
}